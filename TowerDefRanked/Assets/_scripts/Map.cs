﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using All_Variables;


public class Map : MonoBehaviour
{
    public const int sizeX = 16;
    public const int sizeY = 9;
    public int[] starts;
    public int[] endPoints;
    public int[] wall;
    public GameObject[] map = new GameObject[sizeX * sizeY];

    public GameObject prefab;

    bool checkNumber(int _num, int[] _x)
    {
        for (int i = 0; i < _x.Length; i++)
        {
            if (_num == _x[i])
            {
                return true;
            }
        }
        return false;
    }

    void Start()
    {
        for (int y = 0; y < sizeY; y++)
        {
            for (int x = 0; x < sizeX; x++)
            {
                GameObject go = Instantiate(prefab, new Vector3(x -7.5f, y -4f, 0), Quaternion.identity);
                go.name = (x + (sizeX * y)).ToString();
                Node nodeGo = go.GetComponent<Node>();
                if (checkNumber(x + (sizeX * y), starts))
                {
                    nodeGo.NewNode(NODE_TYPE.start);
                }
                else if (checkNumber(x + (sizeX * y), endPoints))
                {
                    nodeGo.NewNode(NODE_TYPE.endpoint);
                }
                else if (checkNumber(x + (sizeX * y), wall))
                {
                    nodeGo.NewNode(NODE_TYPE.wall);
                }
                else
                    nodeGo.NewNode();
                print(x + (sizeX * y));
                //map[x + (sizeX* y)] = go;
            }
        }
    }

    void Update()
    {
        
    }
}
