﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using All_Variables;

public class Node : MonoBehaviour
{
    NODE_TYPE node_type = NODE_TYPE.empty;
    public Sprite[] pic;


    public void NewNode(NODE_TYPE _node_type = NODE_TYPE.empty)
    {
        node_type = _node_type;
        if (node_type == NODE_TYPE.empty) { 
            GetComponent<SpriteRenderer>().sprite = pic[0];
        }
        if (node_type == NODE_TYPE.start)
        {
            GetComponent<SpriteRenderer>().sprite = pic[1];
        }
        if (node_type == NODE_TYPE.endpoint)
        {
            GetComponent<SpriteRenderer>().sprite = pic[2];
        }
        if (node_type == NODE_TYPE.wall)
        {
            GetComponent<SpriteRenderer>().sprite = pic[3];
        }
    }
}
